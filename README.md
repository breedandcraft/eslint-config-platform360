# ESLint Configuration for Platform360 Projects

**Note:** These rules are currently in a provisional state and taken without change from the Console project. It is intended to be a starting point that we can ratify and improve.

## Usage

Add the package to your devDependencies by running the following command or manually adding it to your `package.json`:

```sh
npm install --save-dev eslint-config-platform360
```

Next create a file called `.eslintrc.js` in the root directory of your project that extends this configuration:

```js
module.exports = {
  extends: 'platform360',
};
```

Or for browser based projects where we do not currently use Babel (< ES6):

```js
module.exports = {
  extends: 'platform360/eslintrc-browser',
};
```