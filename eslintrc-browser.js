module.exports = {
	'extends': 'platform360/eslintrc-base',
	'env': {
		'browser': true,
		'jquery': true,
		'es6': false,
	},
	'globals': {
		'_': false,
		'Backbone': false,
		'moment': false,
	},
	'rules': {
		// For now we disable all rules requiring ES6
		'no-var': 'off',
		'prefer-template': 'off',
	},
};
