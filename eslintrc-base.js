module.exports = {
	'extends': 'eslint:recommended',
	'env': {
		'es6': true,
		'node': true
	},
	'rules': {
		'indent': [
			'error',
			'tab',
			{ 'SwitchCase': 1 },
		],
		'linebreak-style': [
			'error',
			'unix',
		],
		'quotes': [
			'error',
			'single',
			{
				'allowTemplateLiterals': true,
			},
		],
		'semi': [
			'error',
			'always',
		],
		'eqeqeq': [
			'error',
			'always',
		],
		'comma-dangle': [
			'error',
			'always-multiline',
		],
		'no-unused-vars': [
			'error',
			{
				'vars': 'all',
				'args': 'after-used',
			},
		],
		'no-dupe-args': 'error',
		'no-dupe-keys': 'error',
		'no-var': 'error',
		'prefer-const': 'error',
		'eol-last': 'error',
		'arrow-parens': [
			'error',
			'always',
		],
		'comma-spacing': [
			'error',
			{
				'before': false,
				'after': true,
			},
		],
		'keyword-spacing': [
			'error',
			{
				'before': true,
				'after': true,
			},
		],
		'space-before-blocks': 'error',
		'no-console': 'off',
		'max-len': [
			'warn',
			{
				'code': 100,
				'ignoreUrls': true,
			},
		],
		'curly': 'error',
		'quote-props': [
			'error',
			'as-needed',
		],
		'space-infix-ops': 'error',
		'prefer-template': 'error',
		'no-trailing-spaces': 'error',
		/*
			We prefer camelCase but in some situations, such as when creating an
			object to write to the DB, we may have to use a different format so
			that we match the name of the columns.
		*/
		'camelcase': [
			'error',
			{
				'properties': 'never',
			},
		],
		/*
		Why?
		It's easier to miss a dot at the end of a line than if it is at the start.

		Correct:
		object
			.property
			.anotherProperty;

		Incorrect:
		object.
			property.
			anotherProperty;
		*/
		'dot-location': [
			'error',
			'property',
		],
	},
};
